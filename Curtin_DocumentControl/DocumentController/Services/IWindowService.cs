﻿namespace DocumentController.Services
{
    public interface IWindowService
    {
        void ShowMessageBox(string message, string caption);
        void ShowWindow(WindowService.TypeOfWindow typeOfWindow, int? parameter = null);
        string ShowDialog(WindowService.TypeOfDialog typeOfDialog);
    }
}