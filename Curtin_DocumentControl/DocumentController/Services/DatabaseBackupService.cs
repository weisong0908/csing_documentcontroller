﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DocumentController.Services
{
    public class DatabaseBackupService : IDatabaseBackupService
    {
        private string originalDatabase;
        private string backupFolder;

        public DatabaseBackupService(string originalDatabase, string backupFolder)
        {
            this.originalDatabase = originalDatabase;
            this.backupFolder = backupFolder;
        }

        public void Backup()
        {
            var now = DateTime.Now;
            var newName = $"Controlled Document Master List - {now.Year}-{now.Month}-{now.Day}-{now.Ticks}.mdb";
            File.Copy(originalDatabase, Path.Combine(backupFolder, newName));
        }
    }
}
