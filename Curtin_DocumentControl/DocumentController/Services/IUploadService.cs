﻿namespace DocumentController.Services
{
    public interface IUploadService
    {
        void Upload(ViewModels.DocumentViewModel document, ViewModels.VersionViewModel version);
    }
}