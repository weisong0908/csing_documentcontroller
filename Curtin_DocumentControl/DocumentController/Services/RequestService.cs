﻿using DocumentController.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;
using System.IO;

namespace DocumentController.Services
{
    public class RequestService : IRequestService
    {
        Word.Application app;
        Word.Document doc;

        string pathToDCR;
        string pathToQAIR;

        public RequestService(string pathToDCR, string pathToQAIR)
        {
            this.pathToDCR = pathToDCR;
            this.pathToQAIR = pathToQAIR;
        }

        public void CreateDCR(DocumentViewModel document)
        {
            object path = GetNewPathOnDesktop(pathToDCR);

            app = new Word.Application();
            doc = app.Documents.Add(ref path);
            app.Visible = true;

            ReplaceText("[date]", DateTime.Today.ToShortDateString());
            ReplaceText("[username]", Environment.UserName);
            ReplaceText("[title]", document.Title);
            ReplaceText("[number]", document.DocumentNumber);
            ReplaceText("[version]", document.VersionNumber);
            ReplaceText("[effectiveDate]", document.EffectiveDate.Value.ToShortDateString());
        }

        public void CreateQAIR()
        {
            object path = GetNewPathOnDesktop(pathToQAIR);

            app = new Word.Application();
            doc = app.Documents.Add(ref path);
            app.Visible = true;

            ReplaceText("[date]", DateTime.Today.ToShortDateString());
            ReplaceText("[username]", Environment.UserName);
        }

        private string GetNewPathOnDesktop(string originalPath)
        {
            var tempFolder = Path.Combine( Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DCR&QAIR");
            Directory.CreateDirectory(tempFolder);
            var newFileName = Path.GetFileNameWithoutExtension(originalPath) + "-" + DateTime.Now.Ticks.ToString() + Path.GetExtension(originalPath);
            var newPath = Path.Combine(tempFolder, newFileName);
            File.Copy(originalPath, newPath);

            return newPath;
        }

        private void ReplaceText(object placeholder, string content)
        {
            Word.Range range = doc.Content;
            range.Find.MatchWholeWord = true;
            range.Find.MatchCase = true;

            if (!range.Find.Execute(placeholder))
                return;

            range.Delete();

            range.Text = (string.IsNullOrEmpty(content)) ? string.Empty : content;
        }
    }
}
