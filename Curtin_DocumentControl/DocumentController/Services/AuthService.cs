﻿using DocumentController.Models;
using DocumentController.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentController.Services
{
    public class AuthService : IAuthService
    {
        private string userName;

        public AuthService(string userName)
        {
            this.userName = userName;
        }

        public bool IsAdmin()
        {
            return true;
        }
    }
}
