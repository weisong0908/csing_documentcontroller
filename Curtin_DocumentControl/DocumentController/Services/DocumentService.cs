﻿using DocumentController.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DocumentController.Services
{
    public class DocumentService: IDocumentService
    {
        private readonly HttpClient client;
        private readonly string serverEndPoint;

        public DocumentService(HttpClient client, string serverBaseUrl)
        {
            this.client = client;
            this.serverEndPoint = serverBaseUrl;
        }

        public async Task<IEnumerable<Document>> GetDocuments()
        {
            var response = await client.GetAsync(serverEndPoint);

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<List<Document>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Document> GetDocument(int id)
        {
            var response = await client.GetAsync($"{serverEndPoint}//{id}");

            if (!response.IsSuccessStatusCode)
                return null;

            var cont = response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Document>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Document> AddDocument(Document document)
        {
            var content = JsonConvert.SerializeObject(document);
            var response = await client.PostAsync(serverEndPoint, new StringContent(content));

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<Document>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Document> UpdateDocument(Document document)
        {
            var content = JsonConvert.SerializeObject(document);
            var response = await client.PutAsync(serverEndPoint, new StringContent(content));

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<Document>(await response.Content.ReadAsStringAsync());
        }

        public void RemoveDocument(Document document)
        {
            throw new NotImplementedException();
        }
    }
}
