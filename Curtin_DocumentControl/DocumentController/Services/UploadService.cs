﻿using DocumentController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace DocumentController.Services
{
    public class UploadService : IUploadService
    {
        private string sharedDrivePath = @"C:\Users\weisong.teng\Desktop\S";
        private string archivePath = @"C:\Users\weisong.teng\Desktop\S\Quality Assurance\#QA & COMPLIANCE Dept Functions#\Controlled Document";

        private ViewModels.DocumentViewModel document;
        private ViewModels.VersionViewModel version;

        private string publicPDF;
        private string publicEditable;
        private string privateCurrentPDF;
        private string privateCurrentEditable;
        private string privateObselete;

        public UploadService(string sharedDrivePath, string archivePath)
        {
            this.sharedDrivePath = sharedDrivePath;
            this.archivePath = archivePath;
        }

        private void SetUp()
        {
            switch (document.Type)
            {
                case Models.Type.Policy:
                    publicPDF = Path.Combine(@"# Curtin Singapore Corporate Policies #", document.Department.ToString());
                    privateCurrentPDF = Path.Combine(document.Department, @"Current\Pol\PDF");
                    privateCurrentEditable = Path.Combine(document.Department, @"Current\Pol\Editable");
                    privateObselete = Path.Combine(document.Department, @"Obselete\Pol");
                    break;
                case Models.Type.Procedure:
                    publicPDF = Path.Combine(@"# Curtin Singapore Corporate Procedures #", document.Department.ToString());
                    privateCurrentPDF = Path.Combine(document.Department, @"Current\Pro\PDF");
                    privateCurrentEditable = Path.Combine(document.Department, @"Current\Pro\Editable");
                    privateObselete = Path.Combine(document.Department, @"Obselete\Pro");
                    break;
                case Models.Type.Form:
                    publicPDF = Path.Combine(@"= Controlled Document =\Forms & Templates", document.Department.ToString());
                    publicEditable = Path.Combine(@"= Controlled Document =\Forms & Templates", document.Department.ToString());
                    privateCurrentPDF = Path.Combine(document.Department, @"Current\F & T\PDF");
                    privateCurrentEditable = Path.Combine(document.Department, @"Current\F & T\Editable");
                    privateObselete = Path.Combine(document.Department, @"Obselete\F & T");
                    break;
                case Models.Type.WorkInstruction:
                    publicPDF = Path.Combine(@"= Controlled Document =\Work Instructions & Guidelines", document.Department.ToString());
                    privateCurrentPDF = Path.Combine(document.Department, @"Current\WI & GL\PDF");
                    privateCurrentEditable = Path.Combine(document.Department, @"Current\WI & GL\Editable");
                    privateObselete = Path.Combine(document.Department, @"Obselete\WI & GL");
                    break;
                case Models.Type.OrganisationChart:
                    publicPDF = Path.Combine(@"= Controlled Document =", "Organisation Chart");
                    privateCurrentPDF = Path.Combine(document.Department, @"Current\Organisation Chart\PDF");
                    privateCurrentEditable = Path.Combine(document.Department, @"Current\Organisation Chart\Editable");
                    privateObselete = Path.Combine(document.Department, @"Obselete\Organisation Chart");
                    break;
            }

            publicPDF = Path.Combine(sharedDrivePath, publicPDF);
            privateCurrentPDF = Path.Combine(archivePath, privateCurrentPDF);
            privateCurrentEditable = Path.Combine(archivePath, privateCurrentEditable);
            privateObselete = Path.Combine(archivePath, privateObselete);

            if (this.document.Type == Models.Type.Form)
            {
                publicPDF = Path.Combine(publicPDF, "PDF");
                publicEditable = Path.Combine(sharedDrivePath, publicEditable);
                publicEditable = Path.Combine(publicEditable, "Editable");
            }
        }

        public void Upload(ViewModels.DocumentViewModel document, ViewModels.VersionViewModel version)
        {
            this.document = document;
            this.version = version;

            SetUp();

            Delete(publicPDF);
            Copy(version.Location_PDF, publicPDF);
            Move(privateCurrentPDF, privateObselete);
            Copy(version.Location_PDF, privateCurrentPDF);
            Move(privateCurrentEditable, privateObselete);
            Copy(version.Location_Editable, privateCurrentEditable);

            if (this.document.Type == Models.Type.Form)
            {
                Delete(publicEditable);
                Copy(version.Location_Editable, publicEditable);
            }
        }

        private void Copy(string sourceFileName, string destinationFolder)
        {
            var fileToCopy = document.Title + " - V" + version.VersionNumber + Path.GetExtension(sourceFileName);
            var destinationFileName = Path.Combine(destinationFolder, fileToCopy);

            if (File.Exists(sourceFileName))
                File.Copy(sourceFileName, destinationFileName);

            Process.Start(destinationFolder);
        }

        private void Delete(string folder)
        {
            var filesToDelete = Directory.GetFiles(folder, document.Title + "*", SearchOption.TopDirectoryOnly);

            if (filesToDelete.Length == 0)
                return;

            File.Delete(filesToDelete[0]);
        }

        private void Move(string sourceFolder, string destinationFolder)
        {
            var searchResults = Directory.GetFiles(sourceFolder, document.Title + "*", SearchOption.TopDirectoryOnly);

            if (searchResults.Length == 0)
                return;

            var fileToMove = Path.GetFileName(searchResults[0]);

            File.Move(Path.Combine(sourceFolder, fileToMove), Path.Combine(destinationFolder, fileToMove));

            Process.Start(sourceFolder);
            Process.Start(destinationFolder);
        }
    }
}
