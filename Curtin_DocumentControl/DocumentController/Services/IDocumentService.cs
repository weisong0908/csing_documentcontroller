﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DocumentController.Models;

namespace DocumentController.Services
{
    public interface IDocumentService
    {
        Task<IEnumerable<Document>> GetDocuments();
        Task<Document> GetDocument(int id);
        Task<Document> AddDocument(Document document);
        Task<Document> UpdateDocument(Document document);
        void RemoveDocument(Document document);
    }
}