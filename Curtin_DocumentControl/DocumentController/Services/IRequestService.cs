﻿using DocumentController.ViewModels;

namespace DocumentController.Services
{
    public interface IRequestService
    {
        void CreateDCR(DocumentViewModel document);
        void CreateQAIR();
    }
}