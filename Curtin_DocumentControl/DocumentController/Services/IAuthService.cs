﻿namespace DocumentController.Services
{
    public interface IAuthService
    {
        bool IsAdmin();
    }
}