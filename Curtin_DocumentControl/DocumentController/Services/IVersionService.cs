﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DocumentController.Services
{
    public interface IVersionService
    {
        Task<IEnumerable<Models.Version>> GetVersions(int documentId);
        Task<Models.Version> GetVersion(int id);
        Task<Models.Version> AddVersion(Models.Version version);
        Task<Models.Version> UpdateVersion(Models.Version version);
        void RemoveVersion(Models.Version version);
    }
}