﻿using DocumentController.Models;
using DocumentController.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DocumentController.Services
{
    public class VersionService : IVersionService
    {
        private readonly HttpClient client;
        private readonly string serverBaseUrl;

        public VersionService(HttpClient client, string serverBaseUrl)
        {
            this.client = client;
            this.serverBaseUrl = serverBaseUrl;
        }

        public async Task<IEnumerable<Models.Version>> GetVersions(int documentId)
        {
            var response = await client.GetAsync($"{serverBaseUrl}/{documentId}");

            if (!response.IsSuccessStatusCode)
                return null;

            var tempValue = @"
            [
                {
                    'id': 1,
                    'versionNumber': '1',
                    'progress': 'Obselete',
                    'documentId': 1
                },
                {
                    'id': 2,
                    'versionNumber': '2',
                    'progress': 'In Effect',
                    'documentId': 1
                }
            ]";

            return JsonConvert.DeserializeObject<List<Models.Version>>(tempValue);
        }

        public async Task<Models.Version> GetVersion(int id)
        {
            var response = await client.GetAsync($"{serverBaseUrl}/{id}");

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<Models.Version>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Models.Version> AddVersion(Models.Version version)
        {
            var content = JsonConvert.SerializeObject(version);
            var response = await client.PostAsync(serverBaseUrl, new StringContent(content));

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<Models.Version>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Models.Version> UpdateVersion(Models.Version version)
        {
            var content = JsonConvert.SerializeObject(version);
            var response = await client.PutAsync(serverBaseUrl, new StringContent(content));

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<Models.Version>(await response.Content.ReadAsStringAsync());
        }

        public void RemoveVersion(Models.Version version)
        {
            throw new NotImplementedException();
        }
    }
}
