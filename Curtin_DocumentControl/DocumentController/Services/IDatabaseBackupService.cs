﻿namespace DocumentController.Services
{
    public interface IDatabaseBackupService
    {
        void Backup();
    }
}