﻿using DocumentController.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Forms = System.Windows.Forms;

namespace DocumentController.Services
{
    public class WindowService : IWindowService
    {
        public void ShowMessageBox(string message, string caption)
        {
            MessageBox.Show(message, caption);
        }

        public void ShowWindow(TypeOfWindow typeOfWindow, int? parameter = null)
        {
            Window window = null;

            switch (typeOfWindow)
            {
                case TypeOfWindow.VersionWindow:
                    window = Application.Current.Windows.OfType<VersionWindow>().SingleOrDefault();

                    if (window == null)
                    {
                        window = new VersionWindow(parameter.Value);
                        window.ShowDialog();
                    }
                    break;
                case TypeOfWindow.About:
                    window = Application.Current.Windows.OfType<AboutWindow>().SingleOrDefault();

                    if (window == null)
                    {
                        window = new AboutWindow();
                        window.Show();
                    }
                    break;
                case TypeOfWindow.NewDocumentWindow:
                    window = Application.Current.Windows.OfType<NewDocumentWindow>().SingleOrDefault();

                    if (window == null)
                    {
                        window = new NewDocumentWindow();
                        window.ShowDialog();
                    }
                    break;
            }

            window.Activate();
        }

        public string ShowDialog(TypeOfDialog typeOfDialog)
        {
            var fileDialog = new Forms.OpenFileDialog();

            switch (typeOfDialog)
            {
                case TypeOfDialog.GetPDF:
                    fileDialog.Filter = "PDF files (*.PDF)|*.PDF";
                    if (fileDialog.ShowDialog() == Forms.DialogResult.OK)
                        return fileDialog.FileName;
                    break;
                case TypeOfDialog.GetEditable:
                    if (fileDialog.ShowDialog() == Forms.DialogResult.OK)
                        return fileDialog.FileName;
                    break;
            }



            return string.Empty;
        }

        public enum TypeOfWindow
        {
            About,
            VersionWindow,
            NewDocumentWindow,
            OpenFileDialog
        }

        public enum TypeOfDialog
        {
            GetPDF,
            GetEditable
        }
    }
}
