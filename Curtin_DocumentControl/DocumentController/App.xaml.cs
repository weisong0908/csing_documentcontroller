﻿using DocumentController.Persistence;
using DocumentController.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using AutoMapper;
using DocumentController.Mapping;
using System.Net.Http;

namespace DocumentController
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public string serverBaseUrl = @"https://my-json-server.typicode.com/weisong0908/JSONServer/";
        private HttpClient client = new HttpClient();

        private string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\csing.navitas.local\shared\Documents\z-Public\QA and Compliance\Controlled Document\Controlled Document Master List.mdb;Persist Security Info=False;Jet OLEDB:Database Password=1234;";
        private string sharedDrivePath = @"\\csing.navitas.local\shared\Documents"; 
        private string archivePath = @"\\csing.navitas.local\shared\Documents\Quality Assurance\#QA & COMPLIANCE Dept Functions#\Controlled Document";
        private string pathToDCRTemplate = @"\\csing.navitas.local\shared\Documents\z-Public\QA and Compliance\Controlled Document\Document Change Request.dotx";
        private string pathToQAIRTemplate = @"\\csing.navitas.local\shared\Documents\z-Public\QA and Compliance\Controlled Document\Quality Assurance Improvement Request.dotx";
        private string pathToDatabase = @"\\csing.navitas.local\shared\Documents\z-Public\QA and Compliance\Controlled Document\Controlled Document Master List.mdb";
        private string pathToDatabaseBackupFolder = @"\\csing.navitas.local\shared\Documents\Quality Assurance\#QA & COMPLIANCE Dept Functions#\Controlled Document\z - Controlled Document Master List backups";
        public IDocumentService DocumentService { get; set; }
        public IVersionService IVersionService { get; set; }

        public IWindowService WindowService { get; set; }
        public IAuthService AuthService { get; set; }
        public IUploadService UploadService { get; set; }
        public IRequestService RequestService { get; set; }
        public IDatabaseBackupService DatabaseBackupService { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            DocumentService = new DocumentService(client, $"{serverBaseUrl}/documents");
            IVersionService = new VersionService(client, $"{serverBaseUrl}/versions");
            WindowService = new WindowService();
            AuthService = new AuthService(Environment.UserName);
            UploadService = new UploadService(sharedDrivePath, archivePath);
            RequestService = new RequestService(pathToDCRTemplate, pathToQAIRTemplate);
            DatabaseBackupService = new DatabaseBackupService(pathToDatabase, pathToDatabaseBackupFolder);

            Mapper.Initialize(config => config.AddProfile<MappingProfile>());

            base.OnStartup(e);

            Window documentWindow = new Views.DocumentWindow();
            documentWindow.Show();
        }
    }
}
