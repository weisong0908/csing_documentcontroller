﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentController.Models
{
    public class Update
    {
        public int Id { get; set; }
        public int DocumentId { get; set; }
        public int VersionId { get; set; }
        public string Field { get; set; }
        public string Author { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime DateUpdate { get; set; }
    }
}
