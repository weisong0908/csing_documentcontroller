﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentController.Models
{
    public static class Type
    {
        public const string Policy = "Policy";
        public const string Procedure = "Procedure";
        public const string Form = "Form";
        public const string WorkInstruction = "Work Instruction";
        public const string OrganisationChart = "Organisation Chart";
    }
}
