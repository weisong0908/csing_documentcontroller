﻿namespace DocumentController.Models
{
    public static class Function
    {
        public const string AcademicServices = "Academic Services";
        public const string AcademicSupport = "Academic Support";
        public const string ExaminationAndAssessments = "Examination and Assessments";
    }
}
