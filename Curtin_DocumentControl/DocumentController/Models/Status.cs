﻿namespace DocumentController.Models
{
    public static class Status
    {
        public const string Active = "Active";
        public const string Retired = "Retired";
    }
}
