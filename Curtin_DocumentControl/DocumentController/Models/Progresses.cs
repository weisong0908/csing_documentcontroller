﻿namespace DocumentController.Models
{
    public static class Progresses
    {
        public const string Proposed = "Proposed";
        public const string QMSApproved = "QMS Approved";
        public const string HODApproved = "HOD Approved";
        public const string PendingUplaod = "Pending Upload";
        public const string InEffect = "In Effect";
        public const string Obsolete = "Obsolete";
    }
}
