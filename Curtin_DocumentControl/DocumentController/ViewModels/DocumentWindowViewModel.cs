﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using DocumentController.Persistence;
using System.Diagnostics;
using DocumentController.Services;
using System.Reflection;
using AutoMapper;
using DocumentController.Models;

namespace DocumentController.ViewModels
{
    public class DocumentWindowViewModel : BaseViewModel
    {
        private readonly IDocumentService documentService;
        private readonly IVersionService versionService;
        private readonly IWindowService windowService;
        private readonly IAuthService authService;
        private readonly IRequestService requestService;
        private readonly IDatabaseBackupService databaseBackupService;
        private readonly IMapper mapper;

        private IList<DocumentViewModel> documents = new ObservableCollection<DocumentViewModel>();

        private IList<DocumentViewModel> _filteredDocuments;
        public IList<DocumentViewModel> FilteredDocuments
        {
            get { return _filteredDocuments; }
            set { SetValue(ref _filteredDocuments, value, nameof(FilteredDocuments)); }
        }

        private int _filteredDocumentsCount;
        public int FilteredDocumentsCount
        {
            get { return _filteredDocumentsCount; }
            set { SetValue(ref _filteredDocumentsCount, value, nameof(FilteredDocumentsCount)); }
        }

        private DocumentViewModel _selectedDocument;
        public DocumentViewModel SelectedDocument
        {
            get { return _selectedDocument; }
            set { SetValue(ref _selectedDocument, value, nameof(SelectedDocument)); }
        }

        private string _documentLocation;
        public string DocumentLocation
        {
            get { return _documentLocation; }
            set { SetValue(ref _documentLocation, value, nameof(DocumentLocation)); }
        }

        public string UserName { get; set; } = Environment.UserName;

        private string _alertMessage;
        public string AlertMessage
        {
            get { return _alertMessage; }
            set { SetValue(ref _alertMessage, value, nameof(AlertMessage)); }
        }

        public string DocumentMenuItemVisibility { get; set; }

        public DocumentWindowViewModel(IDocumentService documentService, IVersionService versionService, IWindowService windowService, IAuthService authService, IRequestService requestService, IDatabaseBackupService databaseBackupService, IMapper mapper)
        {
            this.documentService = documentService;
            this.versionService = versionService;
            this.windowService = windowService;
            this.authService = authService;
            this.requestService = requestService;
            this.databaseBackupService = databaseBackupService;
            this.mapper = mapper;

            FilteredDocuments = new ObservableCollection<DocumentViewModel>();

            DocumentMenuItemVisibility = (authService.IsAdmin()) ? "Visible" : "Collapsed";
        }

        public void OnActivated()
        {
            ResetDocumentList();
        }

        private async void ResetDocumentList()
        {
            var documents = mapper.Map<IList<Document>, IList<DocumentViewModel>>((await documentService.GetDocuments()).ToList());

            if (documents.Count == 0)
                return;

            this.documents.Clear();
            foreach (var document in documents)
                this.documents.Add(document);

            FilteredDocuments = this.documents;
            FilteredDocumentsCount = FilteredDocuments.Count();

            if (SelectedDocument != null)
                OnDocumentSelected(SelectedDocument);
        }

        public void OnSearchTextChanged(string searchText)
        {
            FilteredDocuments = documents.Where(d => d.Title.ToLower().Contains(searchText.ToLower()) || d.DocumentNumber.ToLower().Contains(searchText.ToLower())).ToList();
            FilteredDocumentsCount = FilteredDocuments.Count();
        }

        public async void OnDocumentSelected(DocumentViewModel selectedDocument)
        {
            SelectedDocument = selectedDocument;

            var versions = new List<Models.Version>(await versionService.GetVersions(SelectedDocument.Id));

            var latestEffectiveVersion = versions.Where(v => v.Progress == Progresses.InEffect && v.IsRemoved != "true").OrderByDescending(v => v.EffectiveDate).FirstOrDefault();
            if(latestEffectiveVersion==null)
            {
                SelectedDocument.VersionNumber = string.Empty;
                SelectedDocument.EffectiveDate = null;
            }
            else
            {
                SelectedDocument.VersionNumber = latestEffectiveVersion.VersionNumber;
                SelectedDocument.EffectiveDate = latestEffectiveVersion.EffectiveDate;
            }

            DocumentLocation = GetDocumentLocation();
        }

        private string GetDocumentLocation()
        {
            string sharedDrive = @"\\csing.navitas.local\shared\Documents\";
            string mainFolder = "";
            switch (SelectedDocument.Type)
            {
                case Models.Type.Policy:
                    mainFolder = @"# Curtin Singapore Corporate Policies #\" + SelectedDocument.Department;
                    break;
                case Models.Type.Procedure:
                    mainFolder = @"# Curtin Singapore Corporate Procedures #\" + SelectedDocument.Department;
                    break;
                case Models.Type.Form:
                    mainFolder = @"= Controlled Document =\Forms & Templates\" + SelectedDocument.Department + @"\Editable";
                    break;
                case Models.Type.WorkInstruction:
                    mainFolder = @"= Controlled Document =\Work Instructions & Guidelines\" + SelectedDocument.Department;
                    break;
                case Models.Type.OrganisationChart:
                    mainFolder = @"= Controlled Document =\Organisation Chart";
                    break;
            }

            string folderPath = System.IO.Path.Combine(sharedDrive, mainFolder);
            string[] filesPath = System.IO.Directory.GetFiles(folderPath, $"{SelectedDocument.Title} - V{SelectedDocument.VersionNumber}" + "*", System.IO.SearchOption.TopDirectoryOnly);

            if (filesPath.Length == 0)
                return string.Empty;
            return filesPath[0];
        }

        public void OnNavigateToDocumentLocation()
        {
            var tempStorage = @"\\csing.navitas.local\shared\Documents\z-Public\QA and Compliance\Controlled Document\Temp";
            var fileName = System.IO.Path.GetFileNameWithoutExtension(DocumentLocation) + "-" + DateTime.Now.Ticks.ToString() + System.IO.Path.GetExtension(DocumentLocation);
            var newLocation = System.IO.Path.Combine(tempStorage, fileName);
            System.IO.File.Copy(DocumentLocation, newLocation);
            Process.Start(newLocation);
        }

        public void OnNewDocument()
        {
            windowService.ShowWindow(WindowService.TypeOfWindow.NewDocumentWindow);
        }

        public void OnEditVersion()
        {
            if (_selectedDocument == null)
                return;

            windowService.ShowWindow(WindowService.TypeOfWindow.VersionWindow, _selectedDocument.Id);
        }

        public void ShowAboutWindow()
        {
            windowService.ShowWindow(WindowService.TypeOfWindow.About);
        }

        public void OnGenerateDCR()
        {
            if (_selectedDocument == null)
                return;

            requestService.CreateDCR(SelectedDocument);
        }

        public void OnGenerateQAIR()
        {
            requestService.CreateQAIR();
        }

        public void OnBackUpDatabase()
        {
            databaseBackupService.Backup();
        }
    }
}
