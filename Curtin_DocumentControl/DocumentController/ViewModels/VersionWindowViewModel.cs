﻿using AutoMapper;
using DocumentController.Persistence;
using DocumentController.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DocumentController.ViewModels
{
    public class VersionWindowViewModel: BaseViewModel
    {
        private IDocumentService documentStore;
        private IVersionService versionStore;
        private IWindowService windowService;
        private IUploadService uploadService;
        private IMapper mapper;

        private DocumentViewModel _selectedDocument;
        public DocumentViewModel SelectedDocument
        {
            get { return _selectedDocument; }
            set { SetValue(ref _selectedDocument, value, nameof(SelectedDocument)); }
        }

        private IList<VersionViewModel> _listOfVersion;
        public IList<VersionViewModel> ListOfVersions
        {
            get { return _listOfVersion; }
            set { SetValue(ref _listOfVersion, value, nameof(ListOfVersions)); }
        }

        private VersionViewModel _selectedVersion;
        public VersionViewModel SelectedVersion
        {
            get { return _selectedVersion; }
            set { SetValue(ref _selectedVersion, value, nameof(SelectedVersion)); }
        }

        private int _searchResultsCount;
        public int SearchResultsCount
        {
            get { return _searchResultsCount; }
            set { SetValue(ref _searchResultsCount, value, nameof(SearchResultsCount)); }
        }

        private string _alertMessage;
        public string AlertMessage
        {
            get { return _alertMessage; }
            set { SetValue(ref _alertMessage, value, nameof(AlertMessage)); }
        }

        public IEnumerable<string> Progresses { get; set; } = GetAllFields(typeof(Models.Progresses));

        public string UserName { get; set; } = Environment.UserName;

        public VersionWindowViewModel(int documentId, IDocumentService documentStore, IVersionService versionStore, IWindowService windowService, IUploadService uploadService, IMapper mapper)
        {
            this.documentStore = documentStore;
            this.versionStore = versionStore;
            this.windowService = windowService;
            this.uploadService = uploadService;
            this.mapper = mapper;

            GetDocument(documentId);
        }

        private async void GetDocument(int documentId)
        {
            var doc = await documentStore.GetDocument(documentId);
            SelectedDocument = this.mapper.Map<DocumentViewModel>(await documentStore.GetDocument(documentId));

            ListOfVersions = new ObservableCollection<VersionViewModel>();

            UpdateListOfVersions();

            OnNewVersion();
        }

        private static List<string> GetAllFields(Type type)
        {
            FieldInfo[] fields = type.GetFields();

            var result = new List<string>();

            foreach (FieldInfo field in fields)
                result.Add(field.GetValue(null).ToString());

            return result;
        }

        public async void OnVersionSelected(VersionViewModel selectedVersion)
        {
            SelectedVersion = mapper.Map<VersionViewModel>(await versionStore.GetVersion(selectedVersion.Id));
        }

        public async void OnSaveVersion()
        {
            if (SelectedVersion == null)
                return;

            var version = mapper.Map<Models.Version>(SelectedVersion);

            if (version.Id == 0)
            {
                await versionStore.AddVersion(version);
            }
            else
            {
                await versionStore.UpdateVersion(version);
            }

            UpdateListOfVersions();
        }

        public void OnNewVersion()
        {
            SelectedVersion = new VersionViewModel(SelectedDocument.Id);
        }

        public void OnRemoveVersion()
        {
            if (SelectedVersion == null)
                return;

            var version = mapper.Map<Models.Version>(SelectedVersion);
            version.IsRemoved = "true";

            versionStore.RemoveVersion(version);

            UpdateListOfVersions();

            OnNewVersion();
        }

        public void OnLocatePdf()
        {
            SelectedVersion.Location_PDF = windowService.ShowDialog(WindowService.TypeOfDialog.GetPDF);
        }

        public void OnLocateEditable()
        {
            SelectedVersion.Location_Editable = windowService.ShowDialog(WindowService.TypeOfDialog.GetEditable);
        }

        public void UploadDocument()
        {
            if (!File.Exists(SelectedVersion.Location_Editable) && !File.Exists(SelectedVersion.Location_PDF))
            {
                windowService.ShowMessageBox("There is no document(s) available to be uploaded. Please provide either/both the PDF or editable document(s).", "Document(s) not found");
                return;
            }

            uploadService.Upload(SelectedDocument, SelectedVersion);
        }

        private async void UpdateListOfVersions()
        {
            ListOfVersions.Clear();

            var versions = mapper.Map<IList<VersionViewModel>>((await versionStore.GetVersions(SelectedDocument.Id)).Where(v => v.IsRemoved != "true").OrderByDescending(v => v.EffectiveDate).ToList());
            foreach (var version in versions)
                ListOfVersions.Add(version);

            SearchResultsCount = ListOfVersions.Count;

            int numberOfPendingVersions = versions.Where(v => v.Progress != Models.Progresses.InEffect && v.Progress != Models.Progresses.Obsolete).Count();

            AlertMessage = string.Empty;
            if (numberOfPendingVersions > 0)
                AlertMessage = $"{numberOfPendingVersions} versions in progress";
        }
    }
}
