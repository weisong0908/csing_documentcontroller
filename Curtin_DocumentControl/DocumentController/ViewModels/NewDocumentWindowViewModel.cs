﻿using AutoMapper;
using DocumentController.Models;
using DocumentController.Persistence;
using DocumentController.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DocumentController.ViewModels
{
    public class NewDocumentWindowViewModel : BaseViewModel
    {
        private IDocumentService documentStore;
        private IWindowService windowService;
        private IMapper mapper;

        private DocumentViewModel _newDocument;
        public DocumentViewModel NewDocument
        {
            get { return _newDocument; }
            set { SetValue(ref _newDocument, value, nameof(NewDocument)); }
        }

        private string userName = Environment.UserName;

        public IEnumerable<string> Types { get; set; } = GetAllFields(typeof(Models.Type));
        public IEnumerable<string> Statuses { get; set; } = GetAllFields(typeof(Status));
        public IEnumerable<string> Departments { get; set; } = GetAllFields(typeof(Department));
        private IEnumerable<string> _functions;
        public IEnumerable<string> Functions
        {
            get { return _functions; }
            set { SetValue(ref _functions, value, nameof(Functions)); }
        }

        public NewDocumentWindowViewModel(IDocumentService documentStore, IWindowService windowService, IMapper mapper)
        {
            this.documentStore = documentStore;
            this.windowService = windowService;
            this.mapper = mapper;

            NewDocument = new DocumentViewModel();
        }

        public void UpdateFunctionsBasedOnDepartment(string department)
        {
            var result = new List<string>();
            switch (department)
            {
                case Department.Academic:
                    result.Add(Function.AcademicServices);
                    result.Add(Function.AcademicSupport);
                    result.Add(Function.ExaminationAndAssessments);
                    break;
            }

            Functions = result;            
        }

        public async void OnCreatingNewDocument()
        {
            if (IsValidUpdate())
            {
                Document document = new Document();
                document = mapper.Map<DocumentViewModel, Document>(NewDocument);

                document = await documentStore.AddDocument(document);

                windowService.ShowWindow(WindowService.TypeOfWindow.VersionWindow, document.Id);
            }
        }

        private static List<string> GetAllFields(System.Type type)
        {
            FieldInfo[] fields = type.GetFields();

            var result = new List<string>();

            foreach (FieldInfo field in fields)
                result.Add(field.GetValue(null).ToString());

            return result;
        }

        private bool IsValidUpdate()
        {
            if (string.IsNullOrEmpty(NewDocument.Title))
                return false;

            if (string.IsNullOrEmpty(NewDocument.Type))
                return false;

            if (string.IsNullOrEmpty(NewDocument.Status))
                return false;

            if (string.IsNullOrEmpty(NewDocument.Department))
                return false;

            return true;
        }

        private void GoToEditVersion()
        {
            windowService.ShowWindow(WindowService.TypeOfWindow.VersionWindow, NewDocument.Id);
        }
    }
}
