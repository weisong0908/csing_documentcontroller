﻿using AutoMapper;
using DocumentController.Models;
using DocumentController.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentController.Mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Document, DocumentViewModel>();
            CreateMap<Models.Version, VersionViewModel>();

            CreateMap<DocumentViewModel, Document>();
            CreateMap<VersionViewModel, Models.Version>();
        }
    }
}
