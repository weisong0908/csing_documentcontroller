﻿using System.Collections.Generic;
using DocumentController.Models;

namespace DocumentController.Persistence
{
    public interface IUserStore
    {
        IEnumerable<User> GetUsers();
    }
}