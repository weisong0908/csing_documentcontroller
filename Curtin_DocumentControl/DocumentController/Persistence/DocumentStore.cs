﻿using DocumentController.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace DocumentController.Persistence
{
    public class DocumentStore
    {
        private OleDbConnection _dbConnection;
        private OleDbDataReader _dataReader;
        private OleDbCommand _command;

        public DocumentStore(string connectionString)
        {
            _dbConnection = new OleDbConnection(connectionString);
        }

        public IEnumerable<Document> GetDocuments()
        {
            _dbConnection.Open();
            _command = new OleDbCommand("SELECT ID, Document_No, Document_Title, Department, Document_type, Document_Status FROM Documents", _dbConnection);
            _dataReader = _command.ExecuteReader();

            var documents = new List<Document>();

            while (_dataReader.Read())
            {
                documents.Add(new Document()
                {
                    Id = int.Parse(_dataReader["ID"].ToString()),
                    DocumentNumber = (_dataReader["Document_No"].ToString()),
                    Title = (_dataReader["Document_Title"].ToString()),
                    Department = (_dataReader["Department"].ToString()),
                    Type = (_dataReader["Document_type"].ToString()),
                    Status = (_dataReader["Document_Status"].ToString())
                });
            }

            _dataReader.Close();
            _dbConnection.Close();

            return documents;
        }

        public Document GetDocument(int id)
        {
            _dbConnection.Open();
            _command = new OleDbCommand("SELECT * FROM Documents WHERE ID =" + id, _dbConnection);
            _dataReader = _command.ExecuteReader();

            Document document = null;

            while(_dataReader.Read())
            {
                document = new Document()
                {
                    Id = int.Parse(_dataReader["ID"].ToString()),
                    DocumentNumber = (_dataReader["Document_No"].ToString()),
                    Title = (_dataReader["Document_Title"].ToString()),
                    Department = (_dataReader["Department"].ToString()),
                    Function = (_dataReader["Function"]).ToString(),
                    Type = (_dataReader["Document_type"].ToString()),
                    Status = (_dataReader["Document_Status"].ToString()),
                    Location = (_dataReader["Department"]).ToString()
                };
            }

            _dataReader.Close();
            _dbConnection.Close();

            return document;
        }

        public int AddDocument(Document document)
        {
            document.Id = GetDocuments().OrderBy(d => d.Id).Last().Id + 1;

            string sql = "INSERT INTO Documents " +
                "(ID, Document_No, Document_Title, Department, Function, Document_type, Document_Status) VALUES " +
                "(@Id, @DocumentNumber, @Title, @Department, @Function, @Type, @Status)";

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter Id = new OleDbParameter("@Id", document.Id);
            OleDbParameter documentNumber = new OleDbParameter("@DocumentNumber", document.DocumentNumber);
            OleDbParameter documentTitle = new OleDbParameter("@Title", document.Title);
            OleDbParameter department = new OleDbParameter("@Department", document.Department);
            OleDbParameter function = new OleDbParameter("@Function", document.Function);
            OleDbParameter documentType = new OleDbParameter("@Type", document.Type);
            OleDbParameter documentStatus = new OleDbParameter("@Status", document.Status);

            _command.Parameters.Add(Id);
            _command.Parameters.Add(documentNumber);
            _command.Parameters.Add(documentTitle);
            _command.Parameters.Add(department);
            _command.Parameters.Add(function);
            _command.Parameters.Add(documentType);
            _command.Parameters.Add(documentStatus);

            _command.ExecuteNonQuery();
            _dbConnection.Close();

            return document.Id;
        }

        public void RemoveDocument(Document document)
        {
            throw new NotImplementedException();
        }

        public void UpdateDocument(Document document)
        {
            string sql = "UPDATE Documents SET " +
                "Document_No = @DocumentNumber, " +
                "Document_Title = @Title, " +
                "Department = @Department, " +
                "Function = @Function, " +
                "Document_type = @Type, " +
                "Document_Status = @Status " +
                "WHERE ID = " + document.Id.ToString();

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter documentNumber = new OleDbParameter("@DocumentNumber", document.DocumentNumber);
            OleDbParameter documentTitle = new OleDbParameter("@Title", document.Title);
            OleDbParameter department = new OleDbParameter("@Department", document.Department);
            OleDbParameter function = new OleDbParameter("@Function", document.Function);
            OleDbParameter documentType = new OleDbParameter("@Type", document.Type);
            OleDbParameter documentStatus = new OleDbParameter("@Status", document.Status);

            _command.Parameters.Add(documentNumber);
            _command.Parameters.Add(documentTitle);
            _command.Parameters.Add(department);
            _command.Parameters.Add(function);
            _command.Parameters.Add(documentType);
            _command.Parameters.Add(documentStatus);

            _command.ExecuteNonQuery();
            _dbConnection.Close();
        }
    }
}
