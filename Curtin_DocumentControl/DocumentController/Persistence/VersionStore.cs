﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace DocumentController.Persistence
{
    public class VersionStore
    {
        private OleDbConnection _dbConnection;
        private OleDbDataReader _dataReader;
        private OleDbCommand _command;

        public VersionStore(string connectionString)
        {
            _dbConnection = new OleDbConnection(connectionString);
        }

        public IEnumerable<Models.Version> GetVersions(int documentId)
        {
            _dbConnection.Open();
            _command = new OleDbCommand("SELECT * FROM Versions WHERE Doc_ID =" + documentId, _dbConnection);
            _dataReader = _command.ExecuteReader();

            var versions = new List<Models.Version>();

            while (_dataReader.Read())
            {
                versions.Add(new Models.Version()
                {
                    Id = int.Parse(_dataReader["ID"].ToString()),
                    DocumentId = int.Parse(_dataReader["Doc_ID"].ToString()),
                    VersionNumber = _dataReader["Version"].ToString(),
                    EffectiveDate = (_dataReader["Effective_Date"] == DBNull.Value) ? DateTime.MinValue : DateTime.Parse(_dataReader["Effective_Date"].ToString()),
                    Progress = (_dataReader["Update_Status"].ToString()),
                    IsRemoved = (_dataReader["IsRemoved"].ToString())
                });
            }

            _dataReader.Close();
            _dbConnection.Close();

            return versions;
        }

        public Models.Version GetVersion(int id)
        {
            _dbConnection.Open();
            _command = new OleDbCommand("SELECT * FROM Versions WHERE ID =" + id, _dbConnection);
            _dataReader = _command.ExecuteReader();

            Models.Version version = null;

            while(_dataReader.Read())
            {
                version = new Models.Version()
                {
                    Id = int.Parse(_dataReader["ID"].ToString()),
                    DocumentId = int.Parse(_dataReader["Doc_ID"].ToString()),
                    VersionNumber = _dataReader["Version"].ToString(),
                    EffectiveDate = (_dataReader["Effective_Date"] == DBNull.Value) ? DateTime.MinValue : DateTime.Parse(_dataReader["Effective_Date"].ToString()),
                    Progress = (_dataReader["Update_Status"].ToString()),
                    DescriptionOfChange = (_dataReader["Description_of_Change"].ToString()),
                    PurposeOfChange = (_dataReader["Purpose_of_Change"].ToString()),
                    Requestor = (_dataReader["Requestor"].ToString()),
                    Remarks = (_dataReader["Remarks"].ToString()),
                    Location_PDF = (_dataReader["Location_PDF"].ToString()),
                    Location_Editable = (_dataReader["Location_Editable"].ToString()),
                    IsRemoved = (_dataReader["IsRemoved"].ToString())
                };
            }

            _dataReader.Close();
            _dbConnection.Close();

            return version;
        }

        public void AddVersion(Models.Version version)
        {
            string sql = "INSERT INTO Versions " +
                "(Doc_ID, Version, Effective_Date, Update_Status, Description_of_Change, Purpose_of_Change, " +
                "Requestor, Remarks, Location_PDF, Location_Editable) VALUES " +
                "(@DocumentId, @VersionNumber, @EffectiveDate, @Progress, @DescriptionOfChange, @PurposeOfChange, " +
                "@Requestor, @Remarks, @Location_PDF, @Location_Editable)";

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter documentId = new OleDbParameter("@DocumentId", version.DocumentId);
            OleDbParameter versionNumber = new OleDbParameter("@VersionNumber", version.VersionNumber);
            OleDbParameter effectiveDate = new OleDbParameter("@EffectiveDate", version.EffectiveDate);
            OleDbParameter progress = new OleDbParameter("@Progress", version.Progress);
            OleDbParameter descriptionOfChange = new OleDbParameter("@DescriptionOfChange", version.DescriptionOfChange);
            OleDbParameter purposeOfChange = new OleDbParameter("@PurposeOfChange", version.PurposeOfChange);
            OleDbParameter requestor = new OleDbParameter("@Requestor", version.Requestor);
            OleDbParameter remarks = new OleDbParameter("@Remarks", version.Remarks);
            OleDbParameter location_PDF = new OleDbParameter("@Location_PDF", version.Location_PDF);
            OleDbParameter location_Editable = new OleDbParameter("@Location_Editable", version.Location_Editable);

            _command.Parameters.Add(documentId);
            _command.Parameters.Add(versionNumber);
            _command.Parameters.Add(effectiveDate);
            _command.Parameters.Add(progress);
            _command.Parameters.Add(descriptionOfChange);
            _command.Parameters.Add(purposeOfChange);
            _command.Parameters.Add(requestor);
            _command.Parameters.Add(remarks);
            _command.Parameters.Add(location_PDF);
            _command.Parameters.Add(location_Editable);

            _command.ExecuteNonQuery();
            _dbConnection.Close();
        }

        public void UpdateVersion(Models.Version version)
        {
            string sql = "UPDATE Versions SET " +
                "Doc_ID = @DocumentId, " +
                "Version = @VersionNumber, " +
                "Effective_Date = @EffectiveDate, " +
                "Update_Status = @Progress, " +
                "Description_of_Change = @DescriptionOfChange, " +
                "Purpose_of_Change = @PurposeOfChange, " +
                "Requestor = @Requestor, " +
                "Remarks = @Remarks, " +
                "Location_PDF = @Location_PDF, " +
                "Location_Editable = @Location_Editable " +
                "WHERE ID = " + version.Id;

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter documentId = new OleDbParameter("@DocumentId", version.DocumentId);
            OleDbParameter versionNumber = new OleDbParameter("@VersionNumber", version.VersionNumber);
            OleDbParameter effectiveDate = new OleDbParameter("@EffectiveDate", version.EffectiveDate);
            OleDbParameter progress = new OleDbParameter("@Progress", version.Progress);
            OleDbParameter descriptionOfChange = new OleDbParameter("@DescriptionOfChange", version.DescriptionOfChange);
            OleDbParameter purposeOfChange = new OleDbParameter("@PurposeOfChange", version.PurposeOfChange);
            OleDbParameter requestor = new OleDbParameter("@Requestor", version.Requestor);
            OleDbParameter remarks = new OleDbParameter("@Remarks", version.Remarks);
            OleDbParameter location_PDF = new OleDbParameter("@Location_PDF", version.Location_PDF);
            OleDbParameter location_Editable = new OleDbParameter("@Location_Editable", version.Location_Editable);

            _command.Parameters.Add(documentId);
            _command.Parameters.Add(versionNumber);
            _command.Parameters.Add(effectiveDate);
            _command.Parameters.Add(progress);
            _command.Parameters.Add(descriptionOfChange);
            _command.Parameters.Add(purposeOfChange);
            _command.Parameters.Add(requestor);
            _command.Parameters.Add(remarks);
            _command.Parameters.Add(location_PDF);
            _command.Parameters.Add(location_Editable);

            _command.ExecuteNonQuery();
            _dbConnection.Close();
        }

        public void RemoveVersion(Models.Version version)
        {
            string sql = "UPDATE Versions SET IsRemoved = @IsRemoved WHERE ID = " + version.Id;

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter isRemoved = new OleDbParameter("@IsRemoved", version.IsRemoved);
            _command.Parameters.Add(isRemoved);

            _command.ExecuteNonQuery();
            _dbConnection.Close();
        }
    }
}
