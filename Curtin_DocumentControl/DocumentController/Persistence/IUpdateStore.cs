﻿using System.Collections.Generic;
using DocumentController.Models;

namespace DocumentController.Persistence
{
    public interface IUpdateStore
    {
        void AddUpdate(IList<Update> updates);
    }
}