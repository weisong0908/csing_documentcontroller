﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using DocumentController.Models;

namespace DocumentController.Persistence
{
    public class UserStore : IUserStore
    {
        private OleDbConnection _dbConnection;
        private OleDbDataReader _dataReader;
        private OleDbCommand _command;


        public UserStore(string connectionString)
        {
            _dbConnection = new OleDbConnection(connectionString);
        }

        public IEnumerable<User> GetUsers()
        {
            _dbConnection.Open();
            _command = new OleDbCommand("SELECT ID, UserName, IsAdmin from Users", _dbConnection);
            _dataReader = _command.ExecuteReader();

            var users = new List<User>();

            while(_dataReader.Read())
            {
                users.Add(new User()
                {
                    Id = int.Parse(_dataReader["ID"].ToString()),
                    UserName = _dataReader["UserName"].ToString(),
                    IsAdmin = _dataReader["IsAdmin"].ToString() == "true" ? true : false
                });
            }

            _dataReader.Close();
            _dbConnection.Close();

            return users;
        }
    }
}
