﻿using AutoMapper;
using DocumentController.Persistence;
using DocumentController.Services;
using DocumentController.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocumentController.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class DocumentWindow : Window
    {
        public DocumentWindow()
        {
            InitializeComponent();

            var documentService = (Application.Current as App).DocumentService;
            var versionStore = (Application.Current as App).IVersionService;
            var windowService = (Application.Current as App).WindowService;
            var authService = (Application.Current as App).AuthService;
            var requestService = (Application.Current as App).RequestService;
            var databaseBackupService = (Application.Current as App).DatabaseBackupService;
            var mapper = Mapper.Instance;

            ViewModel = new DocumentWindowViewModel(documentService, versionStore, windowService, authService, requestService, databaseBackupService, mapper);
        }

        public DocumentWindowViewModel ViewModel
        {
            get { return DataContext as DocumentWindowViewModel; }
            set { DataContext = value; }
        }

        private void WatermarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ViewModel.OnSearchTextChanged((e.Source as TextBox).Text.ToString());
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            var selectedDocument = e.AddedItems[0] as DocumentViewModel;

            ViewModel.OnDocumentSelected(selectedDocument);
        }

        private void EditVersion_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnEditVersion();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnNewDocument();
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.ShowAboutWindow();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            ViewModel.OnActivated();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnNavigateToDocumentLocation();
        }

        private void DCR_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnGenerateDCR();
        }

        private void QAIR_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnGenerateQAIR();
        }

        private void DatabaseBackup_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnBackUpDatabase();
        }
    }
}
