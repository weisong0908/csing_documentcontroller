﻿using AutoMapper;
using DocumentController.Services;
using DocumentController.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DocumentController.Views
{
    /// <summary>
    /// Interaction logic for NewDocumentWindow.xaml
    /// </summary>
    public partial class NewDocumentWindow : Window
    {
        public NewDocumentWindow()
        {
            InitializeComponent();

            var documentStore = (Application.Current as App).DocumentService;
            var windowService = (Application.Current as App).WindowService;
            var mapper = Mapper.Instance;

            ViewModel = new NewDocumentWindowViewModel(documentStore, windowService, mapper);
        }

        public NewDocumentWindowViewModel ViewModel
        {
            get { return DataContext as NewDocumentWindowViewModel; }
            set { DataContext = value; }
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnCreatingNewDocument();
        }

        private void Department_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.UpdateFunctionsBasedOnDepartment(e.AddedItems[0].ToString());
        }
    }
}
