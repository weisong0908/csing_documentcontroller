﻿using AutoMapper;
using DocumentController.Persistence;
using DocumentController.Services;
using DocumentController.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DocumentController.Views
{
    /// <summary>
    /// Interaction logic for VersionWindow.xaml
    /// </summary>
    public partial class VersionWindow : Window
    {
        public VersionWindowViewModel ViewModel
        {
            get { return DataContext as VersionWindowViewModel; }
            set { DataContext = value; }
        }

        public VersionWindow(int documentId)
        {
            InitializeComponent();

            var documentStore = (Application.Current as App).DocumentService;
            var versionStore = (Application.Current as App).IVersionService;
            var windowService = (Application.Current as App).WindowService;
            var uploadService = (Application.Current as App).UploadService;
            var mapper = Mapper.Instance;

            ViewModel = new VersionWindowViewModel(documentId, documentStore, versionStore, windowService, uploadService, mapper);
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            var selectedVersion = e.AddedItems[0] as VersionViewModel;

            ViewModel.OnVersionSelected(selectedVersion);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnSaveVersion();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnNewVersion();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnRemoveVersion();
        }

        private void UploadDocument_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.UploadDocument();
        }

        private void FindPdf_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnLocatePdf();
        }

        private void FindEditable_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OnLocateEditable();
        }
    }
}
