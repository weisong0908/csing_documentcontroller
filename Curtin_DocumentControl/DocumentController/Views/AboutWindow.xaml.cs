﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Deployment.Application;

namespace DocumentController.Views
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        Version currentVersion;

        public AboutWindow()
        {
            InitializeComponent();

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                currentVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;

                CurrentVersion.Text = $"Version: {currentVersion.Major}.{currentVersion.Minor}.{currentVersion.Build} (revision {currentVersion.Revision})";
            }
        }
    }
}
